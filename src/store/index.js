import Vue from 'vue';
import Vuex from 'vuex';
import ElementsStore from './elements.module.js';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    elementsStore: ElementsStore,
  },
});
