import dataElements from './data/elements.json';

const state = {
  listElements: [],
};
const getters = {
  getlistElementsInStore: (state) => {
    return state.listElements;
  },
  getElementInStore: (state) => (id) => {
    return state.listElements.filter((element) => element.elementId == id);
  },
};
const mutations = {
  SETLISTELEMENTS: (state, data) => {
    state.listElements = data;
  },
};
const actions = {
  fetchInfosElementsInStore(store) {
    try {
      let data = dataElements;
      if (data) {
        let list = [];
        var i = 1;
        for (const [key, value] of Object.entries(data)) {
          if (key == 'elements') {
            for (var objElement of value) {
              let element = {
                elementId: i,
                nom: objElement,
              };
              list.push(element);
              i++;
            }
            store.commit('SETLISTELEMENTS', list);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  },
};

export default {
  namespaced: true,
  strict: true,
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions,
};
